package com.my.mwp.model;

import com.my.Classes.*;

import java.util.HashMap;
import java.util.Map;

public class BusinessLogic implements Model {

    Florists florist;

    public BusinessLogic() {
        florist = new Florists();

    }

    @Override
    public String createFlowers() {
        //TODO
        Map<AbstractFlower,Integer> flowerMap = new HashMap<>();
        flowerMap.put(new Flower("Roses","red", EFreshness.FIRST_FRESH,30,25.0),50);
        flowerMap.put(new Flower("Roses","white", EFreshness.FIRST_FRESH,30,25.0),50);
        flowerMap.put(new Flowerpot("Flowerdpot","afa", EFreshness.FIRST_FRESH,30,25.0,10,20),25);
        flowerMap.put(new Flowerpot("Flowerafsdpot","addfa", EFreshness.FIRST_FRESH,30,25.0,10,20),25);
        flowerMap.put(new Flowerpot("Flowerpsdfaot","afa", EFreshness.FIRST_FRESH,30,25.0,10,20),25);
        flowerMap.put(new BrushesFlower("Flowerpot","red", EFreshness.FIRST_FRESH,30,25.0,3),10);
        flowerMap.put(new BrushesFlower("fdsafsd","rsfdaed", EFreshness.FIRST_FRESH,30,25.0,3),10);
        flowerMap.put(new BrushesFlower("Floweasdfrpot","ewr", EFreshness.FIRST_FRESH,30,25.0,3),10);
        flowerMap.put(new Flower("qwerty","fas", EFreshness.FIRST_FRESH,30,25.0),50);
        flowerMap.put(new Flower("asdf","red", EFreshness.FIRST_FRESH,30,25.0),50);
        florist.setFlowers(flowerMap);
        return printFlowers();
    }

    @Override
    public String  sellFlowers() {
        Map<AbstractFlower,Integer> sales = new HashMap<>();
        sales.put(new Flower("Roses","white", EFreshness.FIRST_FRESH,30,25.0),5);
        double price = florist.sell(sales);

        String s = "";
        for (AbstractFlower f: sales.keySet()) {
            s += f.toString() + " Count =" +sales.get(f) + "\n";
        }
        s+= " Price "+ price;
        return s;
    }

    @Override
    public String printFlowers() {
        String s = "";
        for (AbstractFlower f: florist.getFlowers().keySet()) {
            s += f.toString() + " Count =" +florist.getFlowers().get(f) + "\n";
        }
        return s;
    }

    @Override
    public String createBouquet() {
        //TODO
        return "";
    }
}

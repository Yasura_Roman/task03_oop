package com.my.mwp.model;

public interface Model {
    String createFlowers();
    String sellFlowers();
    String printFlowers();
    String createBouquet();
}

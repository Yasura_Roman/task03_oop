package com.my.mwp.controller;

public interface Controller {

    String createFlowers();
    String sellFlowers();
    String printFlowers();
    String createBouquet();
}

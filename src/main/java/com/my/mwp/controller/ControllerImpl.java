package com.my.mwp.controller;

import com.my.mwp.model.BusinessLogic;
import com.my.mwp.model.Model;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }


    @Override
    public String createFlowers() {
        return model.createFlowers();
    }

    @Override
    public String sellFlowers() {
        return model.sellFlowers();
    }

    @Override
    public String printFlowers() {
        return model.printFlowers();
    }

    @Override
    public String createBouquet() {
        return model.createBouquet();
    }
}

package com.my.mwp.view;

@FunctionalInterface
public interface Printable {

    void print();
}

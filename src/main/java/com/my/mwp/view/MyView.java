package com.my.mwp.view;

import com.my.mwp.controller.Controller;
import com.my.mwp.controller.ControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print Flowers in shop");
        menu.put("2", "  2 - create Flower in shop ");
        menu.put("3", "  3 - sell Flowers ");
        menu.put("4", "  4 - create Bouquet");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        System.out.println(controller.printFlowers());
    }

    private void pressButton2() {
        System.out.println(controller.createFlowers());
    }

    private void pressButton3() {
        System.out.println(controller.sellFlowers());
    }

    private void pressButton4() {
        System.out.println(controller.createBouquet());
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
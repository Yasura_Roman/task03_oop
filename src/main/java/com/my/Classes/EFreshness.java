package com.my.Classes;

/**
 * This enum show degree freshness of flower.
 */
public enum EFreshness {
    FIRST_FRESH,
    SECOND_FRESH,
    FADED
}

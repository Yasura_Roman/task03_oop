package com.my.Classes;

/**
 * This is class which extends AbstractFlower
 *  * and implements IBouquetFlower.
 */
public class Flower extends AbstractFlower implements IBouquetFlower {

    public Flower() {
    }

    public Flower(String name,String color, EFreshness freshness,
                     int steamLength,double price) {
        this.name = name;
        this.color = color;
        this.freshness = freshness;
        this.steamLength = steamLength;
        this.price = price;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this){
            return true;
        }
        if (!(obj instanceof Flower)){
            return false;
        }
        if (super.equals(obj)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        //TODO
        return super.toString();
    }
}

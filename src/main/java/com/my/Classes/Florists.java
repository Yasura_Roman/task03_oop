package com.my.Classes;

import java.util.HashMap;
import java.util.Map;

public class Florists {
    private String name;
    private Map<AbstractFlower,Integer> flowers;

    public Florists() {
    }

    public Florists(Map<AbstractFlower, Integer> flowers) {
        this.flowers = flowers;
    }

    public boolean isValidSell(Map<AbstractFlower,Integer> bouquet){
        for (AbstractFlower  f: flowers.keySet()) {
            for (AbstractFlower b: bouquet.keySet() ) {
                if (b.equals(f)){
                    if (bouquet.get(b) > flowers.get(f)){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean isValidSell(IBouquet bouquet){
        for (AbstractFlower  f: flowers.keySet()) {
            for (IBouquetFlower b: bouquet.getComposition().keySet() ) {
                if (b.equals(f)){
                    if (bouquet.getComposition().get(b) > flowers.get(f)){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public double sell(IBouquet bouquet) {
        if (isValidSell(bouquet)){
            for (AbstractFlower  f: flowers.keySet()) {
                for (IBouquetFlower b: bouquet.getComposition().keySet() ) {
                    if (b.equals(f)){
                        flowers.put(f,(flowers.get(f)-bouquet.getComposition().get(b)));
                    }
                }
            }
            return bouquet.getPrice();
        }
        return 0;
    }

    public double sell(AbstractFlower flower,int count) {
        for (AbstractFlower  f: flowers.keySet()) {
            if ( f.equals(flower) && !( count > flowers.get(f)) ){
                flowers.put(f,flowers.get(f) - count);
                return f.price * count;
            }
        }
        return 0;
    }

    public double sell(Map<AbstractFlower,Integer> flowersMap) {
        if (isValidSell(flowersMap)){
            double price =0;
            for (AbstractFlower  f: flowers.keySet()) {
                for (AbstractFlower b: flowersMap.keySet() ) {
                    if (b.equals(f)){
                        flowers.put(f,flowers.get(f)-flowersMap.get(b));
                        price += b.price *flowersMap.get(b);
                    }
                }
            }
            return price;
        }
        return 0;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<AbstractFlower, Integer> getFlowers() {
        return flowers;
    }

    public void setFlowers(Map<AbstractFlower, Integer> flowers) {
        this.flowers = flowers;
    }
}

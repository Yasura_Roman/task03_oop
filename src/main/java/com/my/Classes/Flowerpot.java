package com.my.Classes;

/**
 * This class describes the flowerpot.
 * And Extends AbstractFlower.
 */
public class Flowerpot extends AbstractFlower {

    private int size;
    private double weight;

    public Flowerpot(){}

    public Flowerpot(String name,String color, EFreshness freshness,
                          int steamLength,double price, int size, double weight) {
        this.name = name;
        this.color = color;
        this.freshness = freshness;
        this.steamLength = steamLength;
        this.price = price;
        this.size = size;
        this.weight = weight;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this){
            return true;
        }
        if (!(obj instanceof Flowerpot)){
            return false;
        }
        if (super.equals(obj)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        //TODO
        return super.toString();
    }
}

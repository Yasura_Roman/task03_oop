package com.my.Classes;

import java.util.Map;

public interface IBouquet {
    void add(IBouquetFlower flower, int count);
    double getPrice();
    String getCompositionString();
    Map<IBouquetFlower,Integer> getComposition();
}

package com.my.Classes;

/**
 * This class extends AbstractFlower
 * and implements IBouquetFlower.
 */
public class BrushesFlower extends AbstractFlower implements IBouquetFlower {

    private int flowerCount;

    public BrushesFlower(){}

    public BrushesFlower(String name,String color, EFreshness freshness,
                         int steamLength,double price, int flowerCount) {
        this.name = name;
        this.color = color;
        this.freshness = freshness;
        this.steamLength = steamLength;
        this.price = price;
        this.flowerCount = flowerCount;
    }

    public int getFlowerCount() {
        return flowerCount;
    }

    public void setFlowerCount(int flowerCount) {
        this.flowerCount = flowerCount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this){
            return true;
        }
        if (!(obj instanceof BrushesFlower)){
            return false;
        }
        if (super.equals(obj)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        //TODO
        return super.toString();
    }
}

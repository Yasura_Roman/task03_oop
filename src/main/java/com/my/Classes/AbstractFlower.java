package com.my.Classes;

/**
 * This abstract class is base of flower.
 * @author Roman Yasiura
 * @version 1.0
 * @since 10.11.2019
 */
public abstract class AbstractFlower implements Comparable<AbstractFlower> {

    protected String name;
    protected String color;
    protected EFreshness freshness;
    protected int steamLength;
    protected double price;

    public int compareTo(AbstractFlower abstractFlower) {
        //TODO
        int compare = this.name.compareTo(abstractFlower.name);
        if ( compare == 0){
            compare = color.compareTo(abstractFlower.color);
        }
        return compare;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public EFreshness getFreshness() {
        return freshness;
    }

    public void setFreshness(EFreshness freshness) {
        this.freshness = freshness;
    }

    public int getSteamLength() {
        return steamLength;
    }

    public void setSteamLength(int steamLength) {
        this.steamLength = steamLength;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this){
            return true;
        }
        if (!(obj instanceof AbstractFlower)){
            return false;
        }
        return this.compareTo((AbstractFlower) obj) == 0;
    }

    @Override
    public String toString() {
        //TODO
        return "name = " + name + " color = " + color;
    }
}

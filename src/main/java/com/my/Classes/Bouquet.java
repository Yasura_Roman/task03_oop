package com.my.Classes;

import java.util.Map;
import java.util.stream.Collectors;

public class Bouquet implements IBouquet {

    private String compositionName;
    private String packing;
    private Map<IBouquetFlower,Integer>  composition;

    public Bouquet() {
    }

    public Bouquet(String compositionName, String packing, Map<IBouquetFlower, Integer> composition) {
        this.compositionName = compositionName;
        this.packing = packing;
        this.composition = composition;
    }

    public void add(IBouquetFlower flower, int count) {
        composition.put(flower,count);
    }

    public double getPrice() {
        double totalPrice = 0;
        totalPrice += composition.entrySet().stream()
                .mapToDouble((e) -> {
            if (e.getKey() instanceof AbstractFlower){
                return ((AbstractFlower) e.getKey()).getPrice() * (int) e.getValue();
            }
            return 0;
        }).sum();
        return totalPrice;
    }

    public String getCompositionString() {
        return composition.entrySet().stream()
                .map(e ->  e.getValue().toString() +", count= "+ e.getValue().toString())
                .collect(Collectors.joining(","));
    }

    public Map<IBouquetFlower, Integer> getComposition() {
        return composition;
    }
}
